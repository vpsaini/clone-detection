package com.main;

import java.io.IOException;

import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.jobconf_005fhistory_jsp;

import com.gen.stage1.Task1Conf;
import com.gen.stage2.Task2Conf;

public class main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Start");
		JobConf conf = null;
		conf = Task1Conf.getConf("in", "tempout");
		try {
			JobClient.runJob(conf);
			conf = Task2Conf.getConf("tempout", "out");
			JobClient.runJob(conf);
			conf = Task2Conf.getConf("out", "out2");
			JobClient.runJob(conf);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
