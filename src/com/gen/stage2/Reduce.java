package com.gen.stage2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class Reduce extends MapReduceBase implements Reducer<Text, Text, Text, Text> {
	
	class kv{
		
		public String first=null,last=null,firstHalf = null, secondHalf = null;
		
		public kv(String val){
			String temp[] = val.split(":"),middle=null;
			first = temp[0];
			last = temp[temp.length-1];
			StringBuffer sb = new StringBuffer();
			String delimiter="";
			for(int i=1;i<temp.length-1;i++){
				sb.append(delimiter);
				sb.append(temp[i]);
				delimiter=":";
			}
			middle = sb.toString();
			if(middle.equalsIgnoreCase("")){
				firstHalf = first;
				secondHalf = last;
			}else{
				firstHalf = first+":"+sb.toString();
				secondHalf = sb.toString()+":"+last;
			}
			
		}
		
		public boolean isCompatible(kv other){
			if(this.firstHalf.equalsIgnoreCase(other.secondHalf))
			{
	    		return true;
			}
			else if(this.secondHalf.equalsIgnoreCase(other.firstHalf))
			{
				return true;
			}else{
				return false;
			}
		}
		
		public String fuze(kv other){
			StringBuffer sb = new StringBuffer();
			if(this.firstHalf.equalsIgnoreCase(other.secondHalf))
			{
	    		sb.append(other.first);
	    		sb.append(":");
	    		sb.append(this.firstHalf);
	    		sb.append(":");
	    		sb.append(this.last);
			}
			else if(this.secondHalf.equalsIgnoreCase(other.firstHalf))
			{
				sb.append(this.first);
	    		sb.append(":");
	    		sb.append(other.firstHalf);
	    		sb.append(":");
	    		sb.append(other.last);
			}else{
				return "";
			}
			return sb.toString();
		}
	}
	
	
    public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
    	LinkedList<String> _keys = new LinkedList<String>();
    	LinkedList<String> _values = new LinkedList<String>();
    	String temp = null;
    	while(values.hasNext()){
    		temp = values.next().toString();
    		if(temp.startsWith("##")){
    			temp = temp.substring(2);
    			_keys.add(temp);
    		}else{
    			_values.add(temp);
    		}
    	}
    	if(_keys.size()==2){
    		kv typenode1 = new kv(_keys.get(0)), typenode2 = new kv(_keys.get(1));
        	HashMap<String,LinkedList<String>> results = new HashMap<String,LinkedList<String>>();
        	if(typenode1.isCompatible(typenode2)){
        		// possible clone
        		kv value1=null,value2=null;
        		for(int i = 0 ; i<_values.size() ;i++){
        			value1=new kv(_values.get(i));
        			for(int j = i+1 ; j<_values.size() ;j++){
            			value2 = new kv(_values.get(j));
            			if(value1.isCompatible(value2)){
            				// clone identified
//            				System.out.println("Found: "+typenode1.fuze(typenode2)+"##"+value1.fuze(value2));
            				String newvaltype = typenode1.fuze(typenode2), val = value1.fuze(value2);
            				LinkedList<String> vals=null;
            				newvaltype = newvaltype.trim();
            				if(results.containsKey(newvaltype)){
            					vals = results.get(newvaltype);
            				}else{
            					vals = new LinkedList<String>();
            				}
            				vals.add(val);
            				results.put(newvaltype, vals);
            			}else{
            				// do nothing
            			}
            		}
        		}
        		
        		for(Entry<String,LinkedList<String>> entry:results.entrySet()){
        			String newkey = entry.getKey();
        			System.out.println(newkey);
        			LinkedList<String> vals = entry.getValue();
        			if(vals.size()==1){
        				// do nothing.. no clone only 1 of its type
        			}else{
        				StringBuffer sb = new StringBuffer();
//        				sb.append("##");
//        				sb.append(newkey);
        				String delimiter="";
        				for(String newval:vals){
        					sb.append(delimiter);
        					sb.append(newval);
        					delimiter=",";
        				}
    					output.collect(new Text(newkey), new Text(sb.toString()));
        			}
        		}
        		
        	}else{
        		// don't bother
        	}
    	}else{
    		// do nothing
    	}
    	
    }
  }
