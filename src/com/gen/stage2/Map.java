package com.gen.stage2;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import com.gen.stage2.Reduce.kv;

public class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {
	
class kv{
		
		public String first=null,last=null,firstHalf = null, secondHalf = null;
		
		public kv(String val){
			String temp[] = val.split(":"),middle=null;
			first = temp[0];
			last = temp[temp.length-1];
			StringBuffer sb = new StringBuffer();
			String delimiter="";
			for(int i=1;i<temp.length-1;i++){
				sb.append(delimiter);
				sb.append(temp[i]);
				delimiter=":";
			}
			middle = sb.toString();
			if(middle.equalsIgnoreCase("")){
				firstHalf = first;
				secondHalf = last;
			}else{
				firstHalf = first+":"+sb.toString();
				secondHalf = sb.toString()+":"+last;
			}
			
		}
		
		public boolean isCompatible(kv other){
			if(this.firstHalf.equalsIgnoreCase(other.secondHalf))
			{
	    		return true;
			}
			else if(this.secondHalf.equalsIgnoreCase(other.firstHalf))
			{
				return true;
			}else{
				return false;
			}
		}
	}
	
	Text txt = new Text();
  public void configure(JobConf conf){
	  System.out.println("Job2 started");
  }
  
  public void close(){
	  System.out.println("Job2 Ended");
  }

  public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
    String line = value.toString();
    String _key=null,_value=null;
    int firstSpace = line.indexOf("\t");
    _key = line.substring(0, firstSpace);
    _value = line.substring(firstSpace+1);
    kv newkey = new kv(_key);
    String nodes[] = {newkey.firstHalf,newkey.secondHalf};//_key.split(":");
    for(String node:nodes){
    	output.collect(new Text(node), new Text("##"+_key));
    	String temp[] = _value.split(",");
    	for(String st:temp){
    		output.collect(new Text(node), new Text(st));
    	}
    }
  }
}